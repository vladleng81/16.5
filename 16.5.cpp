﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime> 
using namespace std;

int main() {

    setlocale(LC_ALL, "Rus");

    //объявляет наши константу и массив
    const int n = 6;
    int array[n][n];

    //наполненяет массив
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            array[i][j] = i + j;
        }
    }

    //печатает массив
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << array[i][j] << " ";
        }
        cout << endl;
    }

    //определяет день и получает остаток 
    //деления текущего числа календаря на N.
    time_t t;
    time(&t);
    int k = (localtime(&t)->tm_mday) % n;

    //находит сумму всех эелементов нужной строки,
    //а так же печатает её прибавив к номеру строки 1
    //чтобы он отображался верно.
    for (int sum, i = 0; i < n; i++) {
        sum = 0;
        for (int j = 0; j < n; j++) {
            if (array[i][j]) {
                sum += array[k][j];
            }
            else {
                sum = 0;
                break;
            }
        }
        if (sum) {
            cout << "Сумма строки " << k + 1 << " равняется: " << sum << endl;
            break;
        }
    }

    return 0;
}